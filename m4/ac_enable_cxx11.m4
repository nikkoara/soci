dnl -*- Autoconf -*-

AC_DEFUN([AC_ENABLE_CXX11],[
  CXXFLAGS+=' -std=c++11'
])
