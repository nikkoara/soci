dnl -*- Autoconf -*-

AC_DEFUN([AC_PACKAGE_DEFINES],[
    ac_pkg_prefix="$1"

    for h in $ac_config_headers; do
        echo $h | while IFS=':' read f ignore; do
            sed "s,PACKAGE,${ac_pkg_prefix}_PACKAGE," $f >$f.tmp
            mv $f.tmp $f
        done
    done
])
