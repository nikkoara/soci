//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_SOURCE
#include <soci/session.hpp>
#include <soci/connection-parameters.hpp>
#include <soci/connection-pool.hpp>
#include <soci/soci-backend.hpp>
#include <soci/query_transformation.hpp>

using namespace soci;
using namespace soci::details;

namespace { // anonymous

void ensureConnected (session_backend_t* backEnd)
{
    if (backEnd == NULL) {
        throw soci_error_t ("Session_T is not connected.");
    }
}

} // namespace anonymous

session_t::session_t()
    : once (this), prepare (this), query_transformation_ (NULL), logStream_ (NULL),
      uppercaseColumnNames_ (false), backEnd_ (NULL),
      isFromPool_ (false), pool_ (NULL)
{
}

session_t::session_t (connection_parameters_t const& parameters)
    : once (this), prepare (this), query_transformation_ (NULL), logStream_ (NULL),
      lastConnectParameters_ (parameters),
      uppercaseColumnNames_ (false), backEnd_ (NULL),
      isFromPool_ (false), pool_ (NULL)
{
    open (lastConnectParameters_);
}

session_t::session_t (backend_factory_t const& factory,
                  std::string const& connectString)
    : once (this), prepare (this), query_transformation_ (NULL), logStream_ (NULL),
      lastConnectParameters_ (factory, connectString),
      uppercaseColumnNames_ (false), backEnd_ (NULL),
      isFromPool_ (false), pool_ (NULL)
{
    open (lastConnectParameters_);
}

session_t::session_t (std::string const& backendName,
                  std::string const& connectString)
    : once (this), prepare (this), query_transformation_ (NULL), logStream_ (NULL),
      lastConnectParameters_ (backendName, connectString),
      uppercaseColumnNames_ (false), backEnd_ (NULL),
      isFromPool_ (false), pool_ (NULL)
{
    open (lastConnectParameters_);
}

session_t::session_t (std::string const& connectString)
    : once (this), prepare (this), query_transformation_ (NULL), logStream_ (NULL),
      lastConnectParameters_ (connectString),
      uppercaseColumnNames_ (false), backEnd_ (NULL),
      isFromPool_ (false), pool_ (NULL)
{
    open (lastConnectParameters_);
}

session_t::session_t (connection_pool_t& pool)
    : query_transformation_ (NULL), logStream_ (NULL), isFromPool_ (true), pool_ (&pool)
{
    poolPosition_ = pool.lease();
    session_t& pooledSession = pool.at (poolPosition_);

    once.set_session_t (&pooledSession);
    prepare.set_session_t (&pooledSession);
    backEnd_ = pooledSession.get_backend();
}

session_t::~session_t()
{
    if (isFromPool_) {
        pool_->give_back (poolPosition_);
    }
    else {
        delete query_transformation_;
        delete backEnd_;
    }
}

void session_t::open (connection_parameters_t const& parameters)
{
    if (isFromPool_) {
        pool_->at (poolPosition_).open (parameters);
    }
    else {
        if (backEnd_ != NULL) {
            throw soci_error_t ("Cannot open already connected session.");
        }

        backend_factory_t const* const factory = parameters.get_factory();

        if (factory == NULL) {
            throw soci_error_t ("Cannot connect without a valid backend.");
        }

        backEnd_ = factory->make_session (parameters);
        lastConnectParameters_ = parameters;
    }
}

void session_t::open (backend_factory_t const& factory,
                    std::string const& connectString)
{
    open (connection_parameters_t (factory, connectString));
}

void session_t::open (std::string const& backendName,
                    std::string const& connectString)
{
    open (connection_parameters_t (backendName, connectString));
}

void session_t::open (std::string const& connectString)
{
    open (connection_parameters_t (connectString));
}

void session_t::close()
{
    if (isFromPool_) {
        pool_->at (poolPosition_).close();
        backEnd_ = NULL;
    }
    else {
        delete backEnd_;
        backEnd_ = NULL;
    }
}

void session_t::reconnect()
{
    if (isFromPool_) {
        pool_->at (poolPosition_).reconnect();
        backEnd_ = pool_->at (poolPosition_).get_backend();
    }
    else {
        backend_factory_t const* const lastFactory = lastConnectParameters_.get_factory();

        if (lastFactory == NULL) {
            throw soci_error_t ("Cannot reconnect without previous connection.");
        }

        if (backEnd_ != NULL) {
            close();
        }

        backEnd_ = lastFactory->make_session (lastConnectParameters_);
    }
}

void session_t::begin()
{
    ensureConnected (backEnd_);

    backEnd_->begin();
}

void session_t::commit()
{
    ensureConnected (backEnd_);

    backEnd_->commit();
}

void session_t::rollback()
{
    ensureConnected (backEnd_);

    backEnd_->rollback();
}

std::ostringstream& session_t::get_query_stream()
{
    if (isFromPool_) {
        return pool_->at (poolPosition_).get_query_stream();
    }
    else {
        return query_stream_;
    }
}

std::string session_t::get_query() const
{
    if (isFromPool_) {
        return pool_->at (poolPosition_).get_query();
    }
    else {
        // preserve logical constness of get_query,
        // stream used as read-only here,
        session_t* pthis = const_cast<session_t*> (this);

        // sole place where any user-defined query transformation is applied
        if (query_transformation_) {
            return (*query_transformation_) (pthis->get_query_stream().str());
        }

        return pthis->get_query_stream().str();
    }
}


#ifdef SOCI_CXX_C11
void session_t::set_query_transformation_ ( std::unique_ptr<details::query_transformation_function_t>& qtf)
#else
void session_t::set_query_transformation_ ( std::auto_ptr<details::query_transformation_function_t> qtf)
#endif


{
    if (isFromPool_) {
        pool_->at (poolPosition_).set_query_transformation_ (qtf);
    }
    else {
        delete query_transformation_;
        query_transformation_ = qtf.release();
    }
}

void session_t::set_log_stream (std::ostream* s)
{
    if (isFromPool_) {
        pool_->at (poolPosition_).set_log_stream (s);
    }
    else {
        logStream_ = s;
    }
}

std::ostream* session_t::get_log_stream() const
{
    if (isFromPool_) {
        return pool_->at (poolPosition_).get_log_stream();
    }
    else {
        return logStream_;
    }
}

void session_t::log_query (std::string const& query)
{
    if (isFromPool_) {
        pool_->at (poolPosition_).log_query (query);
    }
    else {
        if (logStream_ != NULL) {
            *logStream_ << query << '\n';
        }

        lastQuery_ = query;
    }
}

std::string session_t::get_last_query() const
{
    if (isFromPool_) {
        return pool_->at (poolPosition_).get_last_query();
    }
    else {
        return lastQuery_;
    }
}

void session_t::set_got_data (bool gotData)
{
    if (isFromPool_) {
        pool_->at (poolPosition_).set_got_data (gotData);
    }
    else {
        gotData_ = gotData;
    }
}

bool session_t::got_data() const
{
    if (isFromPool_) {
        return pool_->at (poolPosition_).got_data();
    }
    else {
        return gotData_;
    }
}

void session_t::uppercase_column_names (bool forceToUpper)
{
    if (isFromPool_) {
        pool_->at (poolPosition_).uppercase_column_names (forceToUpper);
    }
    else {
        uppercaseColumnNames_ = forceToUpper;
    }
}

bool session_t::get_uppercase_column_names() const
{
    if (isFromPool_) {
        return pool_->at (poolPosition_).get_uppercase_column_names();
    }
    else {
        return uppercaseColumnNames_;
    }
}

bool session_t::get_next_sequence_value (std::string const& sequence, long& value)
{
    ensureConnected (backEnd_);

    return backEnd_->get_next_sequence_value (*this, sequence, value);
}

bool session_t::get_last_insert_id (std::string const& sequence, long& value)
{
    ensureConnected (backEnd_);

    return backEnd_->get_last_insert_id (*this, sequence, value);
}

std::string session_t::get_backend_name() const
{
    ensureConnected (backEnd_);

    return backEnd_->get_backend_name();
}

statement_backend_t* session_t::make_statement_backend()
{
    ensureConnected (backEnd_);

    return backEnd_->make_statement_backend();
}

rowid_backend_t* session_t::make_rowid_backend()
{
    ensureConnected (backEnd_);

    return backEnd_->make_rowid_backend();
}

blob_backend_t* session_t::make_blob_backend()
{
    ensureConnected (backEnd_);

    return backEnd_->make_blob_backend();
}
