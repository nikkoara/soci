//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_SOURCE
#include <soci/prepare-temp-type.hpp>
#include <soci/ref-counted-prepare-info.hpp>
#include <soci/session.hpp>

using namespace soci;
using namespace soci::details;

prepare_temp_type_t::prepare_temp_type_t (session_t& s)
    : rcpi_ (new ref_counted_prepare_info_t (s))
{
    // this is the beginning of new query
    s.get_query_stream().str ("");
}

prepare_temp_type_t::prepare_temp_type_t (prepare_temp_type_t const& o)
    : rcpi_ (o.rcpi_)
{
    rcpi_->inc_ref();
}

prepare_temp_type_t& prepare_temp_type_t::operator= (prepare_temp_type_t const& o)
{
    o.rcpi_->inc_ref();
    rcpi_->dec_ref();
    rcpi_ = o.rcpi_;

    return *this;
}

prepare_temp_type_t::~prepare_temp_type_t()
{
    rcpi_->dec_ref();
}

prepare_temp_type_t& prepare_temp_type_t::operator, (into_type_ptr const& i)
{
    rcpi_->exchange (i);
    return *this;
}
