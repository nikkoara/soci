//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_SOURCE
#include <soci/rowid.hpp>
#include <soci/session.hpp>

using namespace soci;
using namespace soci::details;

rowid_t::rowid_t (session_t& s)
{
    backEnd_ = s.make_rowid_backend();
}

rowid_t::~rowid_t()
{
    delete backEnd_;
}
