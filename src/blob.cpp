//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_SOURCE
#include <soci/blob.hpp>
#include <soci/session.hpp>

#include <cstddef>

using namespace soci;

blob_t::blob_t (session_t& s)
{
    backEnd_ = s.make_blob_backend();
}

blob_t::~blob_t()
{
    delete backEnd_;
}

std::size_t blob_t::get_len()
{
    return backEnd_->get_len();
}

std::size_t blob_t::read (std::size_t offset, char* buf, std::size_t toRead)
{
    return backEnd_->read (offset, buf, toRead);
}

std::size_t blob_t::write (
    std::size_t offset, char const* buf, std::size_t toWrite)
{
    return backEnd_->write (offset, buf, toWrite);
}

std::size_t blob_t::append (char const* buf, std::size_t toWrite)
{
    return backEnd_->append (buf, toWrite);
}

void blob_t::trim (std::size_t newLen)
{
    backEnd_->trim (newLen);
}
