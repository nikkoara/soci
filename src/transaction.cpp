//
// Copyright (C) 2004-2008 Maciej Sobczak
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_SOURCE

#include <soci/transaction.hpp>
#include <soci/error.hpp>

namespace soci {

transaction_t::transaction_t (session_t& session)
    : session_ (session), complete_ () {
    session_.begin ();
}

transaction_t::~transaction_t () {
    if (!complete_)
        try { rollback (); } catch (...) { }
}

void
transaction_t::commit () {
    if (complete_)
        throw soci_error_t ("error : completed transaction");

    session_.commit ();
    complete_ = true;
}

void
transaction_t::rollback () {
    if (complete_)
        throw soci_error_t ("error : completed transaction");

    session_.rollback ();
    complete_ = true;
}

}
