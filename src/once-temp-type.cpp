//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_SOURCE
#include <soci/once-temp-type.hpp>
#include <soci/ref-counted-statement.hpp>
#include <soci/session.hpp>

using namespace soci;
using namespace soci::details;

once_temp_type_t::once_temp_type_t (session_t& s)
    : rcst_ (new ref_counted_statement_t (s))
{
    // this is the beginning of new query
    s.get_query_stream().str ("");
}

once_temp_type_t::once_temp_type_t (once_temp_type_t const& o)
    : rcst_ (o.rcst_)
{
    rcst_->inc_ref();
}

once_temp_type_t& once_temp_type_t::operator= (once_temp_type_t const& o)
{
    o.rcst_->inc_ref();
    rcst_->dec_ref();
    rcst_ = o.rcst_;

    return *this;
}

once_temp_type_t::~once_temp_type_t() SOCI_ONCE_TEMP_TYPE_NOEXCEPT {
    rcst_->dec_ref();
}

once_temp_type_t& once_temp_type_t::operator, (into_type_ptr const& i)
{
    rcst_->exchange (i);
    return *this;
}
