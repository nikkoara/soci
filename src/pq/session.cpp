//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_POSTGRESQL_SOURCE
#include <soci/soci-platform.hpp>
#include <soci/postgresql/soci-postgresql.hpp>
#include <soci/session.hpp>
#include <soci/connection-parameters.hpp>
#include <libpq/libpq-fs.h> // libpq
#include <cctype>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <sstream>

#ifdef SOCI_POSTGRESQL_NOPARAMS
#ifndef SOCI_POSTGRESQL_NOBINDBYNAME
#define SOCI_POSTGRESQL_NOBINDBYNAME
#endif // SOCI_POSTGRESQL_NOBINDBYNAME
#endif // SOCI_POSTGRESQL_NOPARAMS

using namespace soci;
using namespace soci::details;

namespace { // unnamed

// helper function for hardcoded queries
void hard_exec (PGconn* conn, char const* query, char const* errMsg)
{
    postgresql_result_t (PQexec (conn, query)).check_for_errors (errMsg);
}

} // namespace unnamed

postgresql_session_backend_t::postgresql_session_backend_t (
    connection_parameters_t const& parameters)
    : statementCount_ (0)
{
    PGconn* conn = PQconnectdb (parameters.get_connect_string().c_str());

    if (0 == conn || CONNECTION_OK != PQstatus (conn)) {
        std::string msg = "Cannot establish connection to the database.";

        if (0 != conn) {
            msg += '\n';
            msg += PQerrorMessage (conn);
            PQfinish (conn);
        }

        throw soci_error_t (msg);
    }

    // Increase the number of digits used for floating point values to ensure
    // that the conversions to/from text round trip correctly, which is not the
    // case with the default value of 0. Use the maximal supported value, which
    // was 2 until 9.x and is 3 since it.
    int const version = PQserverVersion (conn);
    hard_exec (conn,
               version >= 90000 ? "SET extra_float_digits = 3"
               : "SET extra_float_digits = 2",
               "Cannot set extra_float_digits parameter");

    conn_ = conn;
}

postgresql_session_backend_t::~postgresql_session_backend_t()
{
    clean_up();
}

void postgresql_session_backend_t::begin()
{
    hard_exec (conn_, "BEGIN", "Cannot begin transaction_t.");
}

void postgresql_session_backend_t::commit()
{
    hard_exec (conn_, "COMMIT", "Cannot commit transaction_t.");
}

void postgresql_session_backend_t::rollback()
{
    hard_exec (conn_, "ROLLBACK", "Cannot rollback transaction_t.");
}

void postgresql_session_backend_t::deallocate_prepared_statement (
    const std::string& statementName)
{
    const std::string& query = "DEALLOCATE " + statementName;

    hard_exec (conn_, query.c_str(),
               "Cannot deallocate prepared statement_t.");
}

bool postgresql_session_backend_t::get_next_sequence_value (
    session_t& s, std::string const& sequence, long& value)
{
    s << "select nextval('" + sequence + "')", into (value);

    return true;
}

void postgresql_session_backend_t::clean_up()
{
    if (0 != conn_) {
        PQfinish (conn_);
        conn_ = 0;
    }
}

std::string postgresql_session_backend_t::get_next_statement_name()
{
    char nameBuf[20] = { 0 }; // arbitrary length
    sprintf (nameBuf, "st_%d", ++statementCount_);
    return nameBuf;
}

postgresql_statement_backend_t* postgresql_session_backend_t::make_statement_backend()
{
    return new postgresql_statement_backend_t (*this);
}

postgresql_rowid_backend_t* postgresql_session_backend_t::make_rowid_backend()
{
    return new postgresql_rowid_backend_t (*this);
}

postgresql_blob_backend_t* postgresql_session_backend_t::make_blob_backend()
{
    return new postgresql_blob_backend_t (*this);
}
