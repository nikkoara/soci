//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_POSTGRESQL_SOURCE
#include <soci/postgresql/soci-postgresql.hpp>
#include <soci/backend-loader.hpp>
#include <libpq/libpq-fs.h> // libpq

#if     defined (SOCI_POSTGRESQL_NOPARAMS)      \
    && !defined (SOCI_POSTGRESQL_NOBINDBYNAME)
#  define SOCI_POSTGRESQL_NOBINDBYNAME
#endif

using namespace soci;
using namespace soci::details;

// concrete factory for Empty concrete strategies
postgresql_session_backend_t* postgresql_backend_factory_t::make_session (
    connection_parameters_t const& parameters) const
{
    return new postgresql_session_backend_t (parameters);
}

postgresql_backend_factory_t const soci::postgresql;

extern "C" {
    // for dynamic backend loading
    const backend_factory_t*
    factory_postgresql ()
    {
        return &soci::postgresql;
    }

    void register_factory_postgresql ()
    {
        soci::dynamic_backends::register_backend (
            "postgresql", soci::postgresql);
    }

} // extern "C"
