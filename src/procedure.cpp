//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_SOURCE
#include <soci/procedure.hpp>
#include <soci/statement.hpp>
#include <soci/prepare-temp-type.hpp>

using namespace soci;
using namespace soci::details;

procedure_impl_t::procedure_impl_t (prepare_temp_type_t const& prep)
    : statement_impl_t (prep.get_prepare_info()->session_),
      refCount_ (1)
{
    ref_counted_prepare_info_t* prepInfo = prep.get_prepare_info();

    // take all bind/define info_t
    intos_.swap (prepInfo->intos_);
    uses_.swap (prepInfo->uses_);

    // allocate handle
    alloc();

    // prepare the statement_t
    prepare (rewrite_for_procedure_call (prepInfo->get_query()));

    define_and_bind();
}
