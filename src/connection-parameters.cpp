//
// Copyright (C) 2013 Vadim Zeitlin
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#define SOCI_SOURCE
#include <soci/connection-parameters.hpp>
#include <soci/soci-backend.hpp>
#include <soci/backend-loader.hpp>

using namespace soci;

namespace { // anonymous

void parseConnectString (std::string const& connectString,
                         std::string& backendName,
                         std::string& connectionParameters)
{
    std::string const protocolSeparator = "://";

    std::string::size_type const p = connectString.find (protocolSeparator);

    if (p == std::string::npos) {
        throw soci_error_t ("No backend name found in " + connectString);
    }

    backendName = connectString.substr (0, p);
    connectionParameters = connectString.substr (p + protocolSeparator.size());
}

} // namespace anonymous

connection_parameters_t::connection_parameters_t()
    : factory_ (NULL)
{
}

connection_parameters_t::connection_parameters_t (backend_factory_t const& factory,
        std::string const& connectString)
    : factory_ (&factory), connectString_ (connectString)
{
}

connection_parameters_t::connection_parameters_t (std::string const& backendName,
        std::string const& connectString)
    : factory_ (&dynamic_backends::get (backendName)), connectString_ (connectString)
{
}

connection_parameters_t::connection_parameters_t (std::string const& fullConnectString)
{
    std::string backendName;
    std::string connectString;

    parseConnectString (fullConnectString, backendName, connectString);

    factory_ = &dynamic_backends::get (backendName);
    connectString_ = connectString;
}
