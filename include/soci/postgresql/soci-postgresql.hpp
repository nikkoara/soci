//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Copyright (C) 2011 Gevorg Voskanyan
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_POSTGRESQL_H_INCLUDED
#define SOCI_POSTGRESQL_H_INCLUDED

#include <soci/soci-backend.hpp>
#include <libpq-fe.h>
#include <vector>

namespace soci {

class postgresql_soci_error_t : public soci_error_t {
public:
    postgresql_soci_error_t (std::string const& msg, char const* sqlst);

    std::string sqlstate() const;

private:
    char sqlstate_[ 5 ];   // not std::string to keep copy-constructor no-throw
};

namespace details {

// A class thinly encapsulating PGresult. Its main purpose is to ensure that
// PQclear() is always called, avoiding result memory leaks.
class postgresql_result_t {
public:
    // Creates a wrapper for the given, possibly NULL, result. The wrapper
    // object takes ownership of the object and will call PQclear() on it.
    explicit postgresql_result_t (PGresult* result = NULL) {
        init (result);
    }

    // Frees any currently stored result pointer and takes ownership of the
    // given one.
    void reset (PGresult* result = NULL) {
        free();
        init (result);
    }

    // Check whether the status is PGRES_COMMAND_OK and throw an exception if
    // it is different. Notice that if the query can return any results,
    // check_for_data() below should be used instead to verify whether anything
    // was returned or not.
    //
    // The provided error message is used only for the exception being thrown
    // and should describe the operation which yielded this result.
    void check_for_errors (char const* errMsg) const;

    // Check whether the status indicates successful query completion, either
    // with the return results (in which case true is returned) or without them
    // (then false is returned). If the status corresponds to an error, throws
    // an exception, just as check_for_errors().
    bool check_for_data (char const* errMsg) const;

    // Implicit conversion to const PGresult: this is somewhat dangerous but
    // allows us to avoid changing the existing code that uses PGresult and
    // avoids the really bad problem with calling PQclear() twice accidentally
    // as this would require a conversion to non-const pointer that we do not
    // provide.
    operator const PGresult* () const {
        return result_;
    }

    // Get the associated result (which may be NULL). Unlike the implicit
    // conversion above, this one returns a non-const pointer, so you should be
    // careful to avoid really modifying it.
    PGresult* get_result() const {
        return result_;
    }

    // Dtor frees the result.
    ~postgresql_result_t() {
        free();
    }

private:
    void init (PGresult* result) {
        result_ = result;
    }

    void free() {
        // Notice that it is safe to call PQclear() with NULL pointer, it
        // simply does nothing in this case.
        PQclear (result_);
    }

    PGresult* result_;

    SOCI_NOT_COPYABLE (postgresql_result_t)
};

} // namespace details

struct postgresql_statement_backend_t;
struct postgresql_standard_into_type_backend_t : details::standard_into_type_backend_t {
    postgresql_standard_into_type_backend_t (postgresql_statement_backend_t& st)
        : statement_ (st) {}

    virtual void define_by_pos (int& position,
                                void* data, details::exchange_type type);

    virtual void pre_fetch();
    virtual void post_fetch (bool gotData, bool calledFromFetch,
                             indicator* ind);

    virtual void clean_up();

    postgresql_statement_backend_t& statement_;

    void* data_;
    details::exchange_type type_;
    int position_;
};

struct postgresql_vector_into_type_backend_t : details::vector_into_type_backend_t {
    postgresql_vector_into_type_backend_t (postgresql_statement_backend_t& st)
        : statement_ (st) {}

    virtual void define_by_pos (int& position,
                                void* data, details::exchange_type type);

    virtual void pre_fetch();
    virtual void post_fetch (bool gotData, indicator* ind);

    virtual void resize (std::size_t sz);
    virtual std::size_t size();

    virtual void clean_up();

    postgresql_statement_backend_t& statement_;

    void* data_;
    details::exchange_type type_;
    int position_;
};

struct postgresql_standard_use_type_backend_t : details::standard_use_type_backend_t {
    postgresql_standard_use_type_backend_t (postgresql_statement_backend_t& st)
        : statement_ (st), position_ (0), buf_ (NULL) {}

    virtual void bind_by_pos (int& position,
                              void* data, details::exchange_type type, bool readOnly);
    virtual void bind_by_name (std::string const& name,
                               void* data, details::exchange_type type, bool readOnly);

    virtual void pre_use (indicator const* ind);
    virtual void post_use (bool gotData, indicator* ind);

    virtual void clean_up();

    postgresql_statement_backend_t& statement_;

    void* data_;
    details::exchange_type type_;
    int position_;
    std::string name_;
    char* buf_;
};

struct postgresql_vector_use_type_backend_t : details::vector_use_type_backend_t {
    postgresql_vector_use_type_backend_t (postgresql_statement_backend_t& st)
        : statement_ (st), position_ (0) {}

    virtual void bind_by_pos (int& position,
                              void* data, details::exchange_type type);
    virtual void bind_by_name (std::string const& name,
                               void* data, details::exchange_type type);

    virtual void pre_use (indicator const* ind);

    virtual std::size_t size();

    virtual void clean_up();

    postgresql_statement_backend_t& statement_;

    void* data_;
    details::exchange_type type_;
    int position_;
    std::string name_;
    std::vector<char*> buffers_;
};

struct postgresql_session_backend_t;
struct postgresql_statement_backend_t : details::statement_backend_t {
    postgresql_statement_backend_t (postgresql_session_backend_t& session_t);
    ~postgresql_statement_backend_t();

    virtual void alloc();
    virtual void clean_up();
    virtual void prepare (std::string const& query,
                          details::statement_type stType);

    virtual exec_fetch_result execute (int number);
    virtual exec_fetch_result fetch (int number);

    virtual long long get_affected_rows();
    virtual int get_number_of_rows();
    virtual std::string get_parameter_name (int index) const;

    virtual std::string rewrite_for_procedure_call (std::string const& query);

    virtual int prepare_for_describe();
    virtual void describe_column (int colNum, data_type& dtype,
                                  std::string& columnName);

    virtual postgresql_standard_into_type_backend_t* make_into_type_backend();
    virtual postgresql_standard_use_type_backend_t* make_use_type_backend();
    virtual postgresql_vector_into_type_backend_t* make_vector_into_type_backend();
    virtual postgresql_vector_use_type_backend_t* make_vector_use_type_backend();

    postgresql_session_backend_t& session_;

    details::postgresql_result_t result_;
    std::string query_;
    details::statement_type stType_;
    std::string statementName_;
    std::vector<std::string> names_; // list of names for named binds

    long long rowsAffectedBulk_; // number of rows affected by the last bulk operation

    int numberOfRows_;  // number of rows retrieved from the server
    int currentRow_;    // "current" row_t number to consume in postFetch
    int rowsToConsume_; // number of rows to be consumed in postFetch

    bool justDescribed_; // to optimize row_t description with immediately
    // following actual statement_t execution

    bool hasIntoElements_;
    bool hasVectorIntoElements_;
    bool hasUseElements_;
    bool hasVectorUseElements_;

    // the following maps are used for finding data buffers according to
    // use elements specified by the user

    typedef std::map<int, char**> UseByPosBuffersMap;
    UseByPosBuffersMap useByPosBuffers_;

    typedef std::map<std::string, char**> UseByNameBuffersMap;
    UseByNameBuffersMap useByNameBuffers_;
};

struct postgresql_rowid_backend_t : details::rowid_backend_t {
    postgresql_rowid_backend_t (postgresql_session_backend_t& session_t);

    ~postgresql_rowid_backend_t();

    unsigned long value_;
};

struct postgresql_blob_backend_t : details::blob_backend_t {
    postgresql_blob_backend_t (postgresql_session_backend_t& session_t);

    ~postgresql_blob_backend_t();

    virtual std::size_t get_len();
    virtual std::size_t read (std::size_t offset, char* buf,
                              std::size_t toRead);
    virtual std::size_t write (std::size_t offset, char const* buf,
                               std::size_t toWrite);
    virtual std::size_t append (char const* buf, std::size_t toWrite);
    virtual void trim (std::size_t newLen);

    postgresql_session_backend_t& session_;

    unsigned long oid_; // oid of the large object
    int fd_;            // descriptor of the large object
};

struct postgresql_session_backend_t : details::session_backend_t {
    postgresql_session_backend_t (connection_parameters_t const& parameters);

    ~postgresql_session_backend_t();

    virtual void begin();
    virtual void commit();
    virtual void rollback();

    void deallocate_prepared_statement (const std::string& statementName);

    virtual bool get_next_sequence_value (session_t& s,
                                          std::string const& sequence, long& value);

    virtual std::string get_backend_name() const {
        return "postgresql";
    }

    void clean_up();

    virtual postgresql_statement_backend_t* make_statement_backend();
    virtual postgresql_rowid_backend_t* make_rowid_backend();
    virtual postgresql_blob_backend_t* make_blob_backend();

    std::string get_next_statement_name();

    int statementCount_;
    PGconn* conn_;
};


struct postgresql_backend_factory_t : backend_factory_t {
    postgresql_backend_factory_t() {}
    virtual postgresql_session_backend_t* make_session (
        connection_parameters_t const& parameters) const;
};

extern postgresql_backend_factory_t const postgresql;

extern "C" {
    // for dynamic backend loading
    backend_factory_t const* factory_postgresql ();
    void register_factory_postgresql ();

} // extern "C"

} // namespace soci

#endif // SOCI_POSTGRESQL_H_INCLUDED
