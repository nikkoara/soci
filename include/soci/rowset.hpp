//
// Copyright (C) 2006-2008 Mateusz Loskot
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_ROWSET_H_INCLUDED
#define SOCI_ROWSET_H_INCLUDED

#include <soci/statement.hpp>
// std
#include <iterator>
#include <memory>

namespace soci {

//
// rowset_t iterator of input category.
//
template <typename T>
class rowset_iterator_t {
public:

    // Standard iterator traits

    typedef std::input_iterator_tag iterator_category;
    typedef T   value_type;
    typedef T* pointer;
    typedef T& reference;
    typedef ptrdiff_t difference_type;

    // Constructors

    rowset_iterator_t()
        : st_ (0), define_ (0) {
    }

    rowset_iterator_t (statement_t& st, T& define)
        : st_ (&st), define_ (&define) {
        // Fetch first row_t to properly initialize iterator
        ++ (*this);
    }

    // Access operators

    reference operator*() const {
        return (*define_);
    }

    pointer operator->() const {
        return & (operator*());
    }

    // Iteration operators

    rowset_iterator_t& operator++() {
        // Fetch next row_t from dataset

        if (st_->fetch() == false) {
            // Set iterator to non-derefencable state (pass-the-end)
            st_ = 0;
            define_ = 0;
        }

        return (*this);
    }

    rowset_iterator_t operator++ (int) {
        rowset_iterator_t tmp (*this);
        ++ (*this);
        return tmp;
    }

    // Comparison operators

    bool operator== (rowset_iterator_t const& rhs) const {
        return (st_ == rhs.st_ && define_ == rhs.define_);
    }

    bool operator!= (rowset_iterator_t const& rhs) const {
        return ((*this == rhs) == false);
    }

private:

    statement_t* st_;
    T* define_;

}; // class rowset_iterator_t

namespace details {

//
// Implementation of rowset_t
//
template <typename T>
class rowset_impl_t {
public:

    typedef rowset_iterator_t<T> iterator;

    rowset_impl_t (details::prepare_temp_type_t const& prep)
        : refs_ (1), st_ (new statement_t (prep)), define_ (new T()) {
        st_->exchange_for_rowset (into (*define_));
        st_->execute();
    }

    void incRef() {
        ++refs_;
    }

    void decRef() {
        if (--refs_ == 0) {
            delete this;
        }
    }

    iterator begin() const {
        // No ownership transfer occurs here
        return iterator (*st_, *define_);
    }

    iterator end() const {
        return iterator();
    }

private:

    unsigned int refs_;

#ifdef SOCI_CXX_C11
    const std::unique_ptr<statement_t> st_;
    const std::unique_ptr<T> define_;
#else
    const std::auto_ptr<statement_t> st_;
    const std::auto_ptr<T> define_;
#endif
    SOCI_NOT_COPYABLE (rowset_impl_t)
}; // class rowset_impl_t

} // namespace details


//
// rowset_t is a thin wrapper on statement_t and provides access to STL-like input iterator.
// The rowset_iterator_t can be used to easily loop through statement_t results and
// use STL algorithms accepting input iterators.
//
template <typename T = soci::row_t>
class rowset_t {
public:

    typedef T value_type;
    typedef rowset_iterator_t<T> iterator;
    typedef rowset_iterator_t<T> const_iterator;

    // this is a conversion constructor
    rowset_t (details::prepare_temp_type_t const& prep)
        : pimpl_ (new details::rowset_impl_t<T> (prep)) {
    }

    rowset_t (rowset_t const& other)
        : pimpl_ (other.pimpl_) {
        pimpl_->incRef();
    }

    ~rowset_t() {
        pimpl_->decRef();
    }

    rowset_t& operator= (rowset_t const& rhs) {
        if (&rhs != this) {
            rhs.pimpl_->incRef();
            pimpl_->decRef();
            pimpl_ = rhs.pimpl_;
        }

        return *this;
    }

    const_iterator begin() const {
        return pimpl_->begin();
    }

    const_iterator end() const {
        return pimpl_->end();
    }

private:

    // Pointer to implementation - the body
    details::rowset_impl_t<T>* pimpl_;

}; // class rowset_t

} // namespace soci

#endif // SOCI_ROWSET_H_INCLUDED
