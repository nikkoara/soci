//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_SESSION_H_INCLUDED
#define SOCI_SESSION_H_INCLUDED

#include <soci/once-temp-type.hpp>
#include <soci/query_transformation.hpp>
#include <soci/connection-parameters.hpp>

// std
#include <cstddef>
#include <memory>
#include <ostream>
#include <sstream>
#include <string>

namespace soci {
class values_t;
class backend_factory_t;

namespace details {

class session_backend_t;
class statement_backend_t;
class rowid_backend_t;
class blob_backend_t;

} // namespace details

class connection_pool_t;

class session_t {
private:

#ifdef SOCI_CXX_C11
    void set_query_transformation_ (std::unique_ptr<details::query_transformation_function_t>& qtf);
#else
    void set_query_transformation_ (std::auto_ptr<details::query_transformation_function_t> qtf);
#endif



public:
    session_t();
    explicit session_t (connection_parameters_t const& parameters);
    session_t (backend_factory_t const& factory, std::string const& connectString);
    session_t (std::string const& backendName, std::string const& connectString);
    explicit session_t (std::string const& connectString);
    explicit session_t (connection_pool_t& pool);

    ~session_t();

    void open (connection_parameters_t const& parameters);
    void open (backend_factory_t const& factory, std::string const& connectString);
    void open (std::string const& backendName, std::string const& connectString);
    void open (std::string const& connectString);
    void close();
    void reconnect();

    void begin();
    void commit();
    void rollback();

    // once and prepare are for syntax sugar only
    details::once_type_t once;
    details::prepare_type_t prepare;

    // even more sugar
    template <typename T>
    details::once_temp_type_t operator<< (T const& t) {
        return once << t;
    }

    std::ostringstream& get_query_stream();
    std::string get_query() const;

    template <typename T>
    void set_query_transformation (T callback) {

#ifdef SOCI_CXX_C11
        std::unique_ptr<details::query_transformation_function_t> qtf (new details::query_transformation_t<T> (callback));
#else
        std::auto_ptr<details::query_transformation_function_t> qtf (new details::query_transformation_t<T> (callback));
#endif
        set_query_transformation_ (qtf);
    }

    // support for basic logging
    void set_log_stream (std::ostream* s);
    std::ostream* get_log_stream() const;

    void log_query (std::string const& query);
    std::string get_last_query() const;

    void set_got_data (bool gotData);
    bool got_data() const;

    void uppercase_column_names (bool forceToUpper);

    bool get_uppercase_column_names() const;

    // Functions for dealing with sequence/auto-increment values.

    // If true is returned, value is filled with the next value from the given
    // sequence. Otherwise either the sequence is invalid (doesn't exist) or
    // the current backend doesn't support sequences. If you use sequences for
    // automatically generating primary key values, you should use
    // get_last_insert_id() after the insertion in this case.
    bool get_next_sequence_value (std::string const& sequence, long& value);

    // If true is returned, value is filled with the last auto-generated value
    // for this table (although some backends ignore the table argument and
    // return the last value auto-generated in this session_t).
    bool get_last_insert_id (std::string const& table, long& value);


    // for diagnostics and advanced users
    // (downcast it to expected back-end session_t class)
    details::session_backend_t* get_backend() {
        return backEnd_;
    }

    std::string get_backend_name() const;

    details::statement_backend_t* make_statement_backend();
    details::rowid_backend_t* make_rowid_backend();
    details::blob_backend_t* make_blob_backend();

private:
    SOCI_NOT_COPYABLE (session_t)

    std::ostringstream query_stream_;
    details::query_transformation_function_t* query_transformation_;

    std::ostream* logStream_;
    std::string lastQuery_;

    connection_parameters_t lastConnectParameters_;

    bool uppercaseColumnNames_;

    details::session_backend_t* backEnd_;

    bool gotData_;

    bool isFromPool_;
    std::size_t poolPosition_;
    connection_pool_t* pool_;
};

} // namespace soci

#endif // SOCI_SESSION_H_INCLUDED
