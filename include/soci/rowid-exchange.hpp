//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_ROWID_EXCHANGE_H_INCLUDED
#define SOCI_ROWID_EXCHANGE_H_INCLUDED

#include <soci/rowid.hpp>
#include <soci/into-type.hpp>
#include <soci/use-type.hpp>
#include <soci/exchange-traits.hpp>
// std
#include <string>

namespace soci {

namespace details {

template <>
class use_type_t<rowid_t> : public standard_use_type_t {
public:
    use_type_t (rowid_t& rid, std::string const& name = std::string())
        : standard_use_type_t (&rid, x_rowid, false, name) {}
    use_type_t (rowid_t const& rid, std::string const& name = std::string())
        : standard_use_type_t (const_cast<rowid_t*> (&rid), x_rowid, true, name) {}
    use_type_t (rowid_t& rid, indicator& ind,
              std::string const& name = std::string())
        : standard_use_type_t (&rid, x_rowid, ind, false, name) {}
    use_type_t (rowid_t const& rid, indicator& ind,
              std::string const& name = std::string())
        : standard_use_type_t (const_cast<rowid_t*> (&rid), x_rowid, ind, true, name) {}
};

template <>
class into_type_t<rowid_t> : public standard_into_type_t {
public:
    into_type_t (rowid_t& rid) : standard_into_type_t (&rid, x_rowid) {}
    into_type_t (rowid_t& rid, indicator& ind)
        : standard_into_type_t (&rid, x_rowid, ind) {}
};

template <>
struct exchange_traits_t<soci::rowid_t> {
    typedef basic_type_tag_t type_family;
};

} // namespace details

} // namespace soci

#endif // SOCI_ROWID_EXCHANGE_H_INCLUDED
