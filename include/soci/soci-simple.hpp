//
// Copyright (C) 2008 Maciej Sobczak
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_SIMPLE_H_INCLUDED
#define SOCI_SIMPLE_H_INCLUDED

#include <soci/soci-platform.hpp>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

// session_t

typedef void* session_handle;
session_handle soci_create_session_t (char const* connectionString);
void soci_destroy_session_t (session_handle s);

void soci_begin (session_handle s);
void soci_commit (session_handle s);
void soci_rollback (session_handle s);

int soci_session_state (session_handle s);
char const* soci_session_error_message (session_handle s);


// blob_t

typedef void* blob_handle;
blob_handle soci_create_blob (session_handle s);
void soci_destroy_blob (blob_handle b);

int soci_blob_get_len (blob_handle b);
int soci_blob_read (blob_handle b, int offset, char* buf, int toRead);
int soci_blob_write (blob_handle b, int offset, char const* buf, int toWrite);
int soci_blob_append (blob_handle b, char const* buf, int toWrite);
int soci_blob_trim (blob_handle b, int newLen);

int soci_blob_state (blob_handle b);
char const* soci_blob_error_message (blob_handle b);


// statement_t

typedef void* statement_handle;
statement_handle soci_create_statement (session_handle s);
void soci_destroy_statement (statement_handle st);

// positional bind of into elments (the functions return the position for convenience)
int soci_into_string   (statement_handle st);
int soci_into_int      (statement_handle st);
int soci_into_long_long (statement_handle st);
int soci_into_double   (statement_handle st);
int soci_into_date     (statement_handle st);
int soci_into_blob     (statement_handle st);

// vector versions
int soci_into_string_v   (statement_handle st);
int soci_into_int_v      (statement_handle st);
int soci_into_long_long_v (statement_handle st);
int soci_into_double_v   (statement_handle st);
int soci_into_date_v     (statement_handle st);

// positional read of into elements
int          soci_get_into_state    (statement_handle st, int position);
char const* soci_get_into_string   (statement_handle st, int position);
int          soci_get_into_int      (statement_handle st, int position);
long long    soci_get_into_long_long (statement_handle st, int position);
double       soci_get_into_double   (statement_handle st, int position);
char const* soci_get_into_date     (statement_handle st, int position);
blob_handle  soci_get_into_blob     (statement_handle st, int position);

// positional (re)size of vectors
int  soci_into_get_size_v (statement_handle st);
void soci_into_resize_v  (statement_handle st, int new_size);

// positional read of vectors
int          soci_get_into_state_v    (statement_handle st, int position, int index);
char const* soci_get_into_string_v   (statement_handle st, int position, int index);
int          soci_get_into_int_v      (statement_handle st, int position, int index);
long long    soci_get_into_long_long_v (statement_handle st, int position, int index);
double       soci_get_into_double_v   (statement_handle st, int position, int index);
char const* soci_get_into_date_v     (statement_handle st, int position, int index);


// named bind of use elements
void soci_use_string   (statement_handle st, char const* name);
void soci_use_int      (statement_handle st, char const* name);
void soci_use_long_long (statement_handle st, char const* name);
void soci_use_double   (statement_handle st, char const* name);
void soci_use_date     (statement_handle st, char const* name);
void soci_use_blob     (statement_handle st, char const* name);

// vector versions
void soci_use_string_v   (statement_handle st, char const* name);
void soci_use_int_v      (statement_handle st, char const* name);
void soci_use_long_long_v (statement_handle st, char const* name);
void soci_use_double_v   (statement_handle st, char const* name);
void soci_use_date_v     (statement_handle st, char const* name);


// named write of use elements
void soci_set_use_state    (statement_handle st, char const* name, int state);
void soci_set_use_string   (statement_handle st, char const* name, char const* val);
void soci_set_use_int      (statement_handle st, char const* name, int val);
void soci_set_use_long_long (statement_handle st, char const* name, long long val);
void soci_set_use_double   (statement_handle st, char const* name, double val);
void soci_set_use_date     (statement_handle st, char const* name, char const* val);
void soci_set_use_blob     (statement_handle st, char const* name, blob_handle blob_t);

// positional (re)size of vectors
int  soci_use_get_size_v (statement_handle st);
void soci_use_resize_v  (statement_handle st, int new_size);

// named write of use vectors
void soci_set_use_state_v (statement_handle st,
                           char const* name, int index, int state);
void soci_set_use_string_v (statement_handle st,
                            char const* name, int index, char const* val);
void soci_set_use_int_v (statement_handle st,
                         char const* name, int index, int val);
void soci_set_use_long_long_v (statement_handle st,
                               char const* name, int index, long long val);
void soci_set_use_double_v (statement_handle st,
                            char const* name, int index, double val);
void soci_set_use_date_v (statement_handle st,
                          char const* name, int index, char const* val);


// named read of use elements (for modifiable use values_t)
int          soci_get_use_state    (statement_handle st, char const* name);
char const* soci_get_use_string   (statement_handle st, char const* name);
int          soci_get_use_int      (statement_handle st, char const* name);
long long    soci_get_use_long_long (statement_handle st, char const* name);
double       soci_get_use_double   (statement_handle st, char const* name);
char const* soci_get_use_date     (statement_handle st, char const* name);
blob_handle  soci_get_use_blob     (statement_handle st, char const* name);


// statement_t preparation and execution
void      soci_prepare (statement_handle st, char const* query);
int       soci_execute (statement_handle st, int withDataExchange);
long long soci_get_affected_rows (statement_handle st);
int       soci_fetch (statement_handle st);
int       soci_got_data (statement_handle st);

int soci_statement_state (statement_handle s);
char const* soci_statement_error_message (statement_handle s);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // SOCI_SIMPLE_H_INCLUDED
