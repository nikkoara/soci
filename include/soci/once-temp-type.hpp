//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_ONCE_TEMP_TYPE_H_INCLUDED
#define SOCI_ONCE_TEMP_TYPE_H_INCLUDED

#include <soci/ref-counted-statement.hpp>
#include <soci/prepare-temp-type.hpp>

#if __cplusplus >= 201103L
#define SOCI_ONCE_TEMP_TYPE_NOEXCEPT noexcept(false)
#else
#define SOCI_ONCE_TEMP_TYPE_NOEXCEPT
#endif

namespace soci {

class session_t;

namespace details {

class ref_counted_statement_t;

// this needs to be lightweight and copyable
class once_temp_type_t {
public:

    once_temp_type_t (session_t& s);
    once_temp_type_t (once_temp_type_t const& o);
    once_temp_type_t& operator= (once_temp_type_t const& o);

    ~once_temp_type_t() SOCI_ONCE_TEMP_TYPE_NOEXCEPT;

    template <typename T>
    once_temp_type_t& operator<< (T const& t) {
        rcst_->accumulate (t);
        return *this;
    }

    once_temp_type_t& operator, (into_type_ptr const&);
    template <typename T, typename Indicator>
    once_temp_type_t& operator, (into_container_t<T, Indicator> const& ic) {
        rcst_->exchange (ic);
        return *this;
    }
    template <typename T, typename Indicator>
    once_temp_type_t& operator, (use_container_t<T, Indicator> const& uc) {
        rcst_->exchange (uc);
        return *this;
    }

private:
    ref_counted_statement_t* rcst_;
};

// this needs to be lightweight and copyable
class once_type_t {
public:
    once_type_t() : session_ (NULL) {}
    once_type_t (session_t* s) : session_ (s) {}

    void set_session_t (session_t* s) {
        session_ = s;
    }

    template <typename T>
    once_temp_type_t operator<< (T const& t) {
        once_temp_type_t o (*session_);
        o << t;
        return o;
    }

private:
    session_t* session_;
};


// this needs to be lightweight and copyable
class prepare_type_t {
public:
    prepare_type_t() : session_ (NULL) {}
    prepare_type_t (session_t* s) : session_ (s) {}

    void set_session_t (session_t* s) {
        session_ = s;
    }

    template <typename T>
    prepare_temp_type_t operator<< (T const& t) {
        prepare_temp_type_t p (*session_);
        p << t;
        return p;
    }

private:
    session_t* session_;
};

} // namespace details

} // namespace soci

#endif
