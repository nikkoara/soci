//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_EXCHANGE_TRAITS_H_INCLUDED
#define SOCI_EXCHANGE_TRAITS_H_INCLUDED

#include <soci/type-conversion-traits.hpp>
#include <soci/soci-backend.hpp>
// std
#include <ctime>
#include <string>
#include <vector>

namespace soci {

namespace details {

struct basic_type_tag_t {};
struct user_type_tag_t {};

template <typename T>
struct exchange_traits_t {
    // this is used for tag-dispatch between implementations for basic types
    // and user-defined types
    typedef user_type_tag_t type_family;

    enum { // anonymous
        x_type =
            exchange_traits_t
            <
            typename type_conversion_t<T>::base_type
            >::x_type
    };
};

template <>
struct exchange_traits_t<short> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_short };
};

template <>
struct exchange_traits_t<unsigned short> : exchange_traits_t<short> {
};

template <>
struct exchange_traits_t<int> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_integer };
};

template <>
struct exchange_traits_t<unsigned int> : exchange_traits_t<int> {
};

template <>
struct exchange_traits_t<char> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_char };
};

template <>
struct exchange_traits_t<long long> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_long_long };
};

template <>
struct exchange_traits_t<unsigned long long> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_unsigned_long_long };
};

// long must be mapped either to x_integer or x_long_long:
template<int long_size> struct long_traits_helper;
template<> struct long_traits_helper<4> {
    enum { x_type = x_integer };
};
template<> struct long_traits_helper<8> {
    enum { x_type = x_long_long };
};

template <>
struct exchange_traits_t<long int> {
    typedef basic_type_tag_t type_family;
    enum { x_type = long_traits_helper<sizeof (long int)>::x_type };
};

template <>
struct exchange_traits_t<unsigned long> : exchange_traits_t<long> {
};

template <>
struct exchange_traits_t<double> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_double };
};

template <>
struct exchange_traits_t<std::string> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_stdstring };
};

template <>
struct exchange_traits_t<std::tm> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_stdtm };
};

template <typename T>
struct exchange_traits_t<std::vector<T> > {
    typedef typename exchange_traits_t<T>::type_family type_family;
    enum { x_type = exchange_traits_t<T>::x_type };
};

} // namespace details

} // namespace soci

#endif // SOCI_EXCHANGE_TRAITS_H_INCLUDED
