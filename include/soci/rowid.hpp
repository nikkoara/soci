//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_ROWID_H_INCLUDED
#define SOCI_ROWID_H_INCLUDED

#include <soci/soci-platform.hpp>

namespace soci {

class session_t;

namespace details {

class rowid_backend_t;

} // namespace details

// ROWID support

class rowid_t {
public:
    explicit rowid_t (session_t& s);
    ~rowid_t();

    details::rowid_backend_t* get_backend() {
        return backEnd_;
    }

private:
    details::rowid_backend_t* backEnd_;
};

} // namespace soci

#endif
