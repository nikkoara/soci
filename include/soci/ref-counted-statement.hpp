//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_REF_COUNTED_STATEMENT_H_INCLUDED
#define SOCI_REF_COUNTED_STATEMENT_H_INCLUDED

#include <soci/statement.hpp>
#include <soci/into-type.hpp>
#include <soci/use-type.hpp>
// std
#include <sstream>

namespace soci {

namespace details {

// this class is a base for both "once" and "prepare" statements
class ref_counted_statement_base_t {
public:
    ref_counted_statement_base_t (session_t& s);

    virtual ~ref_counted_statement_base_t() {}

    virtual void final_action() = 0;

    void inc_ref() {
        ++refCount_;
    }
    void dec_ref() {
        if (--refCount_ == 0) {
            try {
                final_action();
            }
            catch (...) {
                delete this;
                throw;
            }

            delete this;
        }
    }

    template <typename T>
    void accumulate (T const& t) {
        get_query_stream() << t;
    }

protected:
    // this function allows to break the circular dependenc
    // between session_t and this class
    std::ostringstream& get_query_stream();

    int refCount_;

    session_t& session_;

private:
    SOCI_NOT_COPYABLE (ref_counted_statement_base_t)
};

// this class is supposed to be a vehicle for the "once" statements
// it executes the whole statement_t in its destructor
class ref_counted_statement_t : public ref_counted_statement_base_t {
public:
    ref_counted_statement_t (session_t& s)
        : ref_counted_statement_base_t (s), st_ (s) {}

    virtual void final_action();

    template <typename T>
    void exchange (T& t) {
        st_.exchange (t);
    }

private:
    statement_t st_;
};

} // namespace details

} // namespace soci

#endif
