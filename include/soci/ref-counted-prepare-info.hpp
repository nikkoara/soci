//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_REF_COUNTED_PREPARE_INFO_INCLUDED
#define SOCI_REF_COUNTED_PREPARE_INFO_INCLUDED

#include <soci/bind-values.hpp>
#include <soci/ref-counted-statement.hpp>
// std
#include <string>
#include <vector>

namespace soci {

class session_t;

namespace details {

class procedure_impl_t;
class statement_impl_t;
class into_type_base_t;

// this class conveys only the statement_t text and the bind/define info_t
// it exists only to be passed to statement_t's constructor
class ref_counted_prepare_info_t : public ref_counted_statement_base_t {
public:
    ref_counted_prepare_info_t (session_t& s)
        : ref_counted_statement_base_t (s) {
    }

    void exchange (use_type_ptr const& u) {
        uses_.exchange (u);
    }

    template <typename T, typename Indicator>
    void exchange (use_container_t<T, Indicator> const& uc) {
        uses_.exchange (uc);
    }

    void exchange (into_type_ptr const& i) {
        intos_.exchange (i);
    }

    template <typename T, typename Indicator>
    void exchange (into_container_t<T, Indicator> const& ic) {
        intos_.exchange (ic);
    }

    void final_action();

private:
    friend class statement_impl_t;
    friend class procedure_impl_t;

    into_type_vector_t intos_;
    use_type_vector_t  uses_;

    std::string get_query() const;
};

} // namespace details

} // namespace soci

#endif
