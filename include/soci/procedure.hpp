//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_PROCEDURE_H_INCLUDED
#define SOCI_PROCEDURE_H_INCLUDED

#include <soci/statement.hpp>

namespace soci {

namespace details {

class procedure_impl_t : public statement_impl_t {
public:
    procedure_impl_t (session_t& s) : statement_impl_t (s), refCount_ (1) {}
    procedure_impl_t (prepare_temp_type_t const& prep);

    void inc_ref() {
        ++refCount_;
    }
    void dec_ref() {
        if (--refCount_ == 0) {
            delete this;
        }
    }

private:
    int refCount_;
};

} // namespace details

class procedure_t {
public:
    // this is a conversion constructor
    procedure_t (details::prepare_temp_type_t const& prep)
        : impl_ (new details::procedure_impl_t (prep)) {}

    ~procedure_t() {
        impl_->dec_ref();
    }

    // copy is supported here
    procedure_t (procedure_t const& other)
        : impl_ (other.impl_) {
        impl_->inc_ref();
    }
    void operator= (procedure_t const& other) {
        other.impl_->inc_ref();
        impl_->dec_ref();
        impl_ = other.impl_;
    }

    // forwarders to procedure_impl_t
    // (or rather to its base interface from statement_impl_t)

    bool execute (bool withDataExchange = false) {
        gotData_ = impl_->execute (withDataExchange);
        return gotData_;
    }

    bool fetch() {
        gotData_ = impl_->fetch();
        return gotData_;
    }

    bool got_data() const {
        return gotData_;
    }

private:
    details::procedure_impl_t* impl_;
    bool gotData_;
};

} // namespace soci

#endif
