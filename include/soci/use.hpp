//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_USE_H_INCLUDED
#define SOCI_USE_H_INCLUDED

#include <soci/use-type.hpp>
#include <soci/exchange-traits.hpp>
#include <soci/type-conversion.hpp>
#include <soci/soci-backend.hpp>

namespace soci {

namespace details {
template <typename T, typename Indicator>
struct use_container_t {
    use_container_t (T& _t, Indicator& _ind, const std::string& _name)
        : t (_t), ind (_ind), name (_name) {}

    T& t;
    Indicator& ind;
    const std::string& name;
private:
    SOCI_NOT_ASSIGNABLE (use_container_t)
};

typedef void no_indicator;
template <typename T>
struct use_container_t<T, no_indicator> {
    use_container_t (T& _t, const std::string& _name)
        : t (_t), name (_name) {}

    T& t;
    const std::string& name;
private:
    SOCI_NOT_ASSIGNABLE (use_container_t)
};

} // namespace details

template <typename T>
details::use_container_t<T, details::no_indicator> use (T& t, const std::string& name = std::string())
{
    return details::use_container_t<T, details::no_indicator> (t, name);
}

template <typename T>
details::use_container_t<const T, details::no_indicator> use (T const& t, const std::string& name = std::string())
{
    return details::use_container_t<const T, details::no_indicator> (t, name);
}

template <typename T>
details::use_container_t<T, indicator> use (T& t, indicator& ind, std::string const& name = std::string())
{
    return details::use_container_t<T, indicator> (t, ind, name);
}

template <typename T>
details::use_container_t<const T, indicator> use (T const& t, indicator& ind, std::string const& name = std::string())
{
    return details::use_container_t<const T, indicator> (t, ind, name);
}

// vector containers
template <typename T>
details::use_container_t<T, std::vector<indicator> >
use (T& t, std::vector<indicator>& ind, const std::string& name = std::string())
{
    return details::use_container_t<T, std::vector<indicator> > (t, ind, name);
}

template <typename T>
details::use_container_t<std::vector<T>, details::no_indicator >
use (std::vector<T>& t, const std::string& name = std::string())
{
    return details::use_container_t<std::vector<T>, details::no_indicator> (t, name);
}

} // namespace soci

#endif // SOCI_USE_H_INCLUDED
