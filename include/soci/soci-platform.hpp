//
// Copyright (C) 2006-2008 Mateusz Loskot
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_PLATFORM_H_INCLUDED
#define SOCI_PLATFORM_H_INCLUDED

#include <soci/defs.hpp>

#include <stdarg.h>
#include <string.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include <ctime>

#define LL_FMT_FLAGS "ll"

#define SOCI_NOT_ASSIGNABLE(classname)          \
    classname& operator=(const classname&);

#define SOCI_NOT_COPYABLE(classname)            \
    classname(const classname&);                \
    SOCI_NOT_ASSIGNABLE(classname)

#define SOCI_UNUSED(x) (void)x

#endif // SOCI_PLATFORM_H_INCLUDED
