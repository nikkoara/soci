#ifndef SOCI_DEFS_HPP
#define SOCI_DEFS_HPP

#include <soci/config.hpp>

#define SOCI_LIB_PREFIX "soci"
#define SOCI_ABI_VERSION "3.2"
#define SOCI_LIB_SUFFIX "so"

#define SOCI_USE_BOOST 1

#endif // SOCI_DEFS_HPP
