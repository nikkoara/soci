//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_H_INCLUDED
#define SOCI_H_INCLUDED

// namespace soci
#include <soci/soci-platform.hpp>
#include <soci/backend-loader.hpp>
#include <soci/blob.hpp>
#include <soci/blob-exchange.hpp>
#include <soci/connection-pool.hpp>
#include <soci/error.hpp>
#include <soci/exchange-traits.hpp>
#include <soci/into.hpp>
#include <soci/into-type.hpp>
#include <soci/once-temp-type.hpp>
#include <soci/prepare-temp-type.hpp>
#include <soci/procedure.hpp>
#include <soci/ref-counted-prepare-info.hpp>
#include <soci/ref-counted-statement.hpp>
#include <soci/row.hpp>
#include <soci/row-exchange.hpp>
#include <soci/rowid.hpp>
#include <soci/rowid-exchange.hpp>
#include <soci/rowset.hpp>
#include <soci/session.hpp>
#include <soci/soci-backend.hpp>
#include <soci/statement.hpp>
#include <soci/transaction.hpp>
#include <soci/type-conversion.hpp>
#include <soci/type-conversion-traits.hpp>
#include <soci/type-holder.hpp>
#include <soci/type-ptr.hpp>
#include <soci/unsigned-types.hpp>
#include <soci/use.hpp>
#include <soci/use-type.hpp>
#include <soci/values.hpp>
#include <soci/values-exchange.hpp>

// namespace boost
#ifdef SOCI_USE_BOOST
#include <boost/version.hpp>
#include <soci/boost-fusion.hpp>
#include <soci/boost-optional.hpp>
#include <soci/boost-tuple.hpp>
#include <soci/boost-gregorian-date.hpp>
#endif // SOCI_USE_BOOST

#endif // SOCI_H_INCLUDED
