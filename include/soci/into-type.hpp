//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_INTO_TYPE_H_INCLUDED
#define SOCI_INTO_TYPE_H_INCLUDED

#include <soci/soci-backend.hpp>
#include <soci/type-ptr.hpp>
#include <soci/exchange-traits.hpp>
// std
#include <cstddef>
#include <vector>

namespace soci {

class session_t;

namespace details {

class prepare_temp_type_t;
class standard_into_type_backend_t;
class vector_into_type_backend_t;
class statement_impl_t;

// this is intended to be a base class for all classes that deal with
// defining output data
class into_type_base_t {
public:
    virtual ~into_type_base_t() {}

    virtual void define (statement_impl_t& st, int& position) = 0;
    virtual void pre_fetch() = 0;
    virtual void post_fetch (bool gotData, bool calledFromFetch) = 0;
    virtual void clean_up() = 0;

    virtual std::size_t size() const = 0;  // returns the number of elements
    virtual void resize (std::size_t /* sz */) {} // used for vectors only
};

typedef type_ptr_t<into_type_base_t> into_type_ptr;

// standard types

class standard_into_type_t : public into_type_base_t {
public:
    standard_into_type_t (void* data, exchange_type type)
        : data_ (data), type_ (type), ind_ (NULL), backEnd_ (NULL) {}
    standard_into_type_t (void* data, exchange_type type, indicator& ind)
        : data_ (data), type_ (type), ind_ (&ind), backEnd_ (NULL) {}

    virtual ~standard_into_type_t();

protected:
    virtual void post_fetch (bool gotData, bool calledFromFetch);

private:
    virtual void define (statement_impl_t& st, int& position);
    virtual void pre_fetch();
    virtual void clean_up();

    virtual std::size_t size() const {
        return 1;
    }

    // conversion hook (from base type to arbitrary user type)
    virtual void convert_from_base() {}

    void* data_;
    exchange_type type_;
    indicator* ind_;

    standard_into_type_backend_t* backEnd_;
};

// into type base class for vectors
class vector_into_type_t : public into_type_base_t {
public:
    vector_into_type_t (void* data, exchange_type type)
        : data_ (data), type_ (type), indVec_ (NULL), backEnd_ (NULL) {}

    vector_into_type_t (void* data, exchange_type type,
                      std::vector<indicator>& ind)
        : data_ (data), type_ (type), indVec_ (&ind), backEnd_ (NULL) {}

    ~vector_into_type_t();

protected:
    virtual void post_fetch (bool gotData, bool calledFromFetch);

private:
    virtual void define (statement_impl_t& st, int& position);
    virtual void pre_fetch();
    virtual void clean_up();
    virtual void resize (std::size_t sz);
    virtual std::size_t size() const;

    void* data_;
    exchange_type type_;
    std::vector<indicator>* indVec_;

    vector_into_type_backend_t* backEnd_;

    virtual void convert_from_base() {}
};

// implementation for the basic types (those which are supported by the library
// out of the box without user-provided conversions)

template <typename T>
class into_type_t : public standard_into_type_t {
public:
    into_type_t (T& t)
        : standard_into_type_t (&t,
                              static_cast<exchange_type> (exchange_traits_t<T>::x_type)) {}
    into_type_t (T& t, indicator& ind)
        : standard_into_type_t (&t,
                              static_cast<exchange_type> (exchange_traits_t<T>::x_type), ind) {}
};

template <typename T>
class into_type_t<std::vector<T> > : public vector_into_type_t {
public:
    into_type_t (std::vector<T>& v)
        : vector_into_type_t (&v,
                            static_cast<exchange_type> (exchange_traits_t<T>::x_type)) {}
    into_type_t (std::vector<T>& v, std::vector<indicator>& ind)
        : vector_into_type_t (&v,
                            static_cast<exchange_type> (exchange_traits_t<T>::x_type), ind) {}
};

// helper dispatchers for basic types

template <typename T>
into_type_ptr do_into (T& t, basic_type_tag_t)
{
    return into_type_ptr (new into_type_t<T> (t));
}

template <typename T>
into_type_ptr do_into (T& t, indicator& ind, basic_type_tag_t)
{
    return into_type_ptr (new into_type_t<T> (t, ind));
}

template <typename T>
into_type_ptr do_into (T& t, std::vector<indicator>& ind, basic_type_tag_t)
{
    return into_type_ptr (new into_type_t<T> (t, ind));
}

} // namespace details

} // namespace soci

#endif // SOCI_INTO_TYPE_H_INCLUDED
