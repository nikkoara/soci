//
// Copyright (C) 2004-2008 Maciej Sobczak
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_TRANSACTION_H_INCLUDED
#define SOCI_TRANSACTION_H_INCLUDED

#include <soci/soci-platform.hpp>
#include <soci/session.hpp>

namespace soci {

struct transaction_t {
    explicit transaction_t (session_t&);
    ~transaction_t ();

    void commit ();
    void rollback ();

private:
    session_t& session_;
    bool complete_;
};

} // namespace soci

#endif // SOCI_TRANSACTION_H_INCLUDED
