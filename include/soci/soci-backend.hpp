//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_BACKEND_H_INCLUDED
#define SOCI_BACKEND_H_INCLUDED

#include <soci/soci-platform.hpp>
#include <soci/error.hpp>
// std
#include <cstddef>
#include <map>
#include <string>

namespace soci {

// data types, as seen by the user
enum data_type {
    dt_string, dt_date, dt_double, dt_integer, dt_long_long, dt_unsigned_long_long, dt_blob
};

// the enum type for indicator variables
enum indicator { i_ok, i_null, i_truncated };

class session_t;

namespace details {

// data types, as used to describe exchange format
enum exchange_type {
    x_char,
    x_stdstring,
    x_short,
    x_integer,
    x_long_long,
    x_unsigned_long_long,
    x_double,
    x_stdtm,
    x_statement,
    x_rowid,
    x_blob
};

// type of statement_t (used for optimizing statement_t preparation)
enum statement_type {
    st_one_time_query,
    st_repeatable_query
};

// polymorphic into type backend

class standard_into_type_backend_t {
public:
    standard_into_type_backend_t() {}
    virtual ~standard_into_type_backend_t() {}

    virtual void define_by_pos (int& position, void* data, exchange_type type) = 0;

    virtual void pre_fetch() = 0;
    virtual void post_fetch (bool gotData, bool calledFromFetch, indicator* ind) = 0;

    virtual void clean_up() = 0;

private:
    SOCI_NOT_COPYABLE (standard_into_type_backend_t)
};

class vector_into_type_backend_t {
public:

    vector_into_type_backend_t() {}
    virtual ~vector_into_type_backend_t() {}

    virtual void define_by_pos (int& position, void* data, exchange_type type) = 0;

    virtual void pre_fetch() = 0;
    virtual void post_fetch (bool gotData, indicator* ind) = 0;

    virtual void resize (std::size_t sz) = 0;
    virtual std::size_t size() = 0;

    virtual void clean_up() = 0;

private:
    SOCI_NOT_COPYABLE (vector_into_type_backend_t)
};

// polymorphic use type backend

class standard_use_type_backend_t {
public:
    standard_use_type_backend_t() {}
    virtual ~standard_use_type_backend_t() {}

    virtual void bind_by_pos (int& position, void* data,
                              exchange_type type, bool readOnly) = 0;
    virtual void bind_by_name (std::string const& name,
                               void* data, exchange_type type, bool readOnly) = 0;

    virtual void pre_use (indicator const* ind) = 0;
    virtual void post_use (bool gotData, indicator* ind) = 0;

    virtual void clean_up() = 0;

private:
    SOCI_NOT_COPYABLE (standard_use_type_backend_t)
};

class vector_use_type_backend_t {
public:
    vector_use_type_backend_t() {}
    virtual ~vector_use_type_backend_t() {}

    virtual void bind_by_pos (int& position, void* data, exchange_type type) = 0;
    virtual void bind_by_name (std::string const& name,
                               void* data, exchange_type type) = 0;

    virtual void pre_use (indicator const* ind) = 0;

    virtual std::size_t size() = 0;

    virtual void clean_up() = 0;

private:
    SOCI_NOT_COPYABLE (vector_use_type_backend_t)
};

// polymorphic statement_t backend

class statement_backend_t {
public:
    statement_backend_t() {}
    virtual ~statement_backend_t() {}

    virtual void alloc() = 0;
    virtual void clean_up() = 0;

    virtual void prepare (std::string const& query, statement_type eType) = 0;

    enum exec_fetch_result {
        ef_success,
        ef_no_data
    };

    virtual exec_fetch_result execute (int number) = 0;
    virtual exec_fetch_result fetch (int number) = 0;

    virtual long long get_affected_rows() = 0;
    virtual int get_number_of_rows() = 0;

    virtual std::string get_parameter_name (int index) const = 0;

    virtual std::string rewrite_for_procedure_call (std::string const& query) = 0;

    virtual int prepare_for_describe() = 0;
    virtual void describe_column (int colNum, data_type& dtype,
                                  std::string& column_name) = 0;

    virtual standard_into_type_backend_t* make_into_type_backend() = 0;
    virtual standard_use_type_backend_t* make_use_type_backend() = 0;
    virtual vector_into_type_backend_t* make_vector_into_type_backend() = 0;
    virtual vector_use_type_backend_t* make_vector_use_type_backend() = 0;

private:
    SOCI_NOT_COPYABLE (statement_backend_t)
};

// polymorphic RowID backend

class rowid_backend_t {
public:
    virtual ~rowid_backend_t() {}
};

// polymorphic blob_t backend

class blob_backend_t {
public:
    blob_backend_t() {}
    virtual ~blob_backend_t() {}

    virtual std::size_t get_len() = 0;
    virtual std::size_t read (std::size_t offset, char* buf,
                              std::size_t toRead) = 0;
    virtual std::size_t write (std::size_t offset, char const* buf,
                               std::size_t toWrite) = 0;
    virtual std::size_t append (char const* buf, std::size_t toWrite) = 0;
    virtual void trim (std::size_t newLen) = 0;

private:
    SOCI_NOT_COPYABLE (blob_backend_t)
};

// polymorphic session_t backend

class session_backend_t {
public:
    session_backend_t() {}
    virtual ~session_backend_t() {}

    virtual void begin() = 0;
    virtual void commit() = 0;
    virtual void rollback() = 0;

    // At least one of these functions is usually not implemented for any given
    // backend as RDBMS support either sequences or auto-generated values_t, so
    // we don't declare them as pure virtuals to avoid having to define trivial
    // versions of them in the derived classes. However every backend should
    // define at least one of them to allow the code using auto-generated values_t
    // to work.
    virtual bool get_next_sequence_value (session_t&, std::string const&, long&) {
        return false;
    }
    virtual bool get_last_insert_id (session_t&, std::string const&, long&) {
        return false;
    }

    virtual std::string get_backend_name() const = 0;

    virtual statement_backend_t* make_statement_backend() = 0;
    virtual rowid_backend_t* make_rowid_backend() = 0;
    virtual blob_backend_t* make_blob_backend() = 0;

private:
    SOCI_NOT_COPYABLE (session_backend_t)
};

} // namespace details

// simple base class for the session_t back-end factory

class connection_parameters_t;

class backend_factory_t {
public:
    backend_factory_t() {}
    virtual ~backend_factory_t() {}

    virtual details::session_backend_t* make_session (
        connection_parameters_t const& parameters) const = 0;
};

} // namespace soci

#endif // SOCI_BACKEND_H_INCLUDED
