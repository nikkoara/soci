//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_PREPARE_TEMP_TYPE_INCLUDED
#define SOCI_PREPARE_TEMP_TYPE_INCLUDED

#include <soci/into-type.hpp>
#include <soci/use-type.hpp>
#include <soci/use.hpp>
#include <soci/ref-counted-prepare-info.hpp>

namespace soci {

namespace details {

// this needs to be lightweight and copyable
class prepare_temp_type_t {
public:
    prepare_temp_type_t (session_t&);
    prepare_temp_type_t (prepare_temp_type_t const&);
    prepare_temp_type_t& operator= (prepare_temp_type_t const&);

    ~prepare_temp_type_t();

    template <typename T>
    prepare_temp_type_t& operator<< (T const& t) {
        rcpi_->accumulate (t);
        return *this;
    }

    prepare_temp_type_t& operator, (into_type_ptr const& i);

    template <typename T, typename Indicator>
    prepare_temp_type_t& operator, (into_container_t<T, Indicator> const& ic) {
        rcpi_->exchange (ic);
        return *this;
    }
    template <typename T, typename Indicator>
    prepare_temp_type_t& operator, (use_container_t<T, Indicator> const& uc) {
        rcpi_->exchange (uc);
        return *this;
    }

    ref_counted_prepare_info_t* get_prepare_info() const {
        return rcpi_;
    }


private:
    ref_counted_prepare_info_t* rcpi_;
};

} // namespace details

} // namespace soci

#endif
