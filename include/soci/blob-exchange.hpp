//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_BLOB_EXCHANGE_H_INCLUDED
#define SOCI_BLOB_EXCHANGE_H_INCLUDED

#include <soci/blob.hpp>
#include <soci/into-type.hpp>
#include <soci/use-type.hpp>
// std
#include <string>

namespace soci {

namespace details {

template <>
class into_type_t<blob_t> : public standard_into_type_t {
public:
    into_type_t (blob_t& b) : standard_into_type_t (&b, x_blob) {}
    into_type_t (blob_t& b, indicator& ind)
        : standard_into_type_t (&b, x_blob, ind) {}
};

template <>
class use_type_t<blob_t> : public standard_use_type_t {
public:
    use_type_t (blob_t& b, std::string const& name = std::string())
        : standard_use_type_t (&b, x_blob, false, name) {}
    use_type_t (blob_t const& b, std::string const& name = std::string())
        : standard_use_type_t (const_cast<blob_t*> (&b), x_blob, true, name) {}
    use_type_t (blob_t& b, indicator& ind,
              std::string const& name = std::string())
        : standard_use_type_t (&b, x_blob, ind, false, name) {}
    use_type_t (blob_t const& b, indicator& ind,
              std::string const& name = std::string())
        : standard_use_type_t (const_cast<blob_t*> (&b), x_blob, ind, true, name) {}
};

template <>
struct exchange_traits_t<soci::blob_t> {
    typedef basic_type_tag_t type_family;
    enum { x_type = x_blob };
};

} // namespace details

} // namespace soci

#endif // SOCI_BLOB_EXCHANGE_H_INCLUDED
