//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_INTO_ROW_H_INCLUDED
#define SOCI_INTO_ROW_H_INCLUDED

#include <soci/into-type.hpp>
#include <soci/exchange-traits.hpp>
#include <soci/row.hpp>
#include <soci/statement.hpp>
// std
#include <cstddef>

namespace soci {

namespace details {

// Support selecting into a row_t for dynamic queries

template <>
class into_type_t<row_t>
    : public into_type_base_t { // bypass the standard_into_type_t
public:
    into_type_t (row_t& r) : r_ (r) {}
    into_type_t (row_t& r, indicator&) : r_ (r) {}

private:
    // special handling for Row
    virtual void define (statement_impl_t& st, int& /* position */) {
        st.set_row (&r_);

        // actual row_t description is performed
        // as part of the statement_t execute
    }

    virtual void pre_fetch() {}
    virtual void post_fetch (bool gotData, bool /* calledFromFetch */) {
        r_.reset_get_counter();

        if (gotData) {
            // this is used only to re-dispatch to derived class, if any
            // (the derived class might be generated automatically by
            // user conversions)
            convert_from_base();
        }
    }

    virtual void clean_up() {}

    virtual std::size_t size() const {
        return 1;
    }

    virtual void convert_from_base() {}

    row_t& r_;

    SOCI_NOT_COPYABLE (into_type_t)
};

template <>
struct exchange_traits_t<row_t> {
    typedef basic_type_tag_t type_family;
};

} // namespace details

} // namespace soci

#endif
