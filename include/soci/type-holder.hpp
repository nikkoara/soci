//
// Copyright (C) 2004-2008 Maciej Sobczak, Stephen Hutton
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef SOCI_TYPE_HOLDER_H_INCLUDED
#define SOCI_TYPE_HOLDER_H_INCLUDED
// std
#include <typeinfo>

namespace soci {

namespace details {

// Base class holder_t + derived class type_holder_t for storing type data
// instances in a container of holder_t objects
template <typename T>
class type_holder_t;

class holder_t {
public:
    holder_t() {}
    virtual ~holder_t() {}

    template<typename T>
    T get() {
        type_holder_t<T>* p = dynamic_cast<type_holder_t<T> *> (this);

        if (p) {
            return p->template value<T>();
        }
        else {
            throw std::bad_cast();
        }
    }

private:

    template<typename T>
    T value();
};

template <typename T>
class type_holder_t : public holder_t {
public:
    type_holder_t (T* t) : t_ (t) {}
    ~type_holder_t() {
        delete t_;
    }

    template<typename TypeValue>
    TypeValue value() const {
        return *t_;
    }

private:
    T* t_;
};

} // namespace details

} // namespace soci

#endif // SOCI_TYPE_HOLDER_H_INCLUDED
